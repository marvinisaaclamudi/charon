<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class DefaultController extends AbstractController
{
    private string $appName;

    public function __construct(string $appName)
    {
        $this->appName = $appName;
    }

    /**
     * @Route("/",
     *      name="default",
     *      methods={"GET"}
     * )
     */
    public function default() : Response
    {
        $response = [
            'app_name' => $this->appName,
            'message' => 'OK',
        ];
        return $this->json($response, 200);
    }
}