# Charon

## What is Project Charon?
In mythology, Charon was a ferryman who guided souls across the river Styx to the underworld. Project Charon, in the same way, guides requests to their appropriate endpoint.